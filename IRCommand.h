#ifndef ircommand_h
#define ircommand_h

#include <Arduino.h>
#include <IRsend.h>

class IRCommand
{
    public:
        IRCommand(byte pin);
        String send(uint16_t command[], int arraysize);
    private:
        IRsend *_irsend;
};

#endif