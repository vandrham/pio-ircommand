# pio-ircommand

platformio.ini:

  * lib_deps = git@bitbucket.org:vandrham/pio-ircommand.git

```
#include <stdint.h>
#include <Arduino.h>
#include <IRCommand.h>

IRCommand irCommand(PIN_NUMBER);

uint16_t ircode[24] = {6550, 2525, 3300, 1650, 800, 800, 800, 1650, 1650, 850, 1650, 850, 800, 850, 800, 1650, 1650, 850, 800, 850, 800, 825, 1650, 65535};

void send()
{
	String ret = rfCommand.send(ircode, sizeof(ircode));
}
```
