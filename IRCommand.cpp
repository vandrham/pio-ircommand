#include "IRCommand.h"
#include <stdint.h>
#include <Arduino.h>
#include <IRremoteESP8266.h>
#include <IRsend.h>

IRCommand::IRCommand(byte pin)
{
	static IRsend irsend(pin);
	_irsend = &irsend;

    _irsend -> begin();
}

String IRCommand::send(uint16_t command[], int arraysize)
{
    int len = arraysize / sizeof(uint16_t);

    String log = "[";
    uint16_t gccommand[arraysize + 3];

    gccommand[0] = 38000;
    log += gccommand[0];
    log += ",";
    gccommand[1] = 1;
    log += gccommand[1];
    log += ",";
    gccommand[2] = 1;
    log += gccommand[2];

    for (int i = 0; i < len; i++)
    {
        long longval = command[i];
        longval = longval * 38;
        float floatval = longval / 1000;

        gccommand[i + 3] = floatval;

        log += ",";
        log += gccommand[i + 3];
    }

    log += "]";
    

    _irsend -> sendRaw(command, arraysize, 38);
    //delay(700);
    return log;
}
